const gulp = require('gulp'),
  babel = require('gulp-babel'),
  $ = require('gulp-load-plugins')();


// -------------------- PATHS--------------------//
const buildAssets = '../build',
  themeAssets = '../theme/assets',
  snippetsDir = '../theme/snippets',
  sourceNodeModules = './node_modules';


const paths = {
  themeDir: themeAssets,
  snippetsDir: snippetsDir,
  buildScss: buildAssets + '/scss/**/*.scss',
  sass: buildAssets + '/scss/app.scss',
  sassWatch: [
    buildAssets + '/scss/*.scss',
    buildAssets + '/scss/**/*.scss'
  ],
  buildJs: buildAssets + '/js/**/*.js',
  jsWatch: [
    buildAssets + '/js/*.js',
    buildAssets + '/js/**/*.js'
  ],
  buildJsLibs: sourceNodeModules + '/jquery/dist/jquery.js'
};

const catchError = function (error) {
  console.log(error.toString(error));
  this.emit('end');
  if (process.env.EXIT_ON_ERROR == 1) {
    process.exit(1);
  }
  this.emit('end');
}

// -------------------- TASKS--------------------//
const scssLint = (done) => {
  return (
    gulp
      .src(paths.buildScss)
      .pipe($.changed('css'))
      .pipe($.scssLint({
        'maxBuffer': 300000 * 1024,
        'config': 'scsslint.yml'
      }))
      .pipe($.scssLint.failReporter())
  ), done();
};

const sass = (done) => {
  return (
    gulp
      .src(paths.sass)
      .pipe($.sass({
        errLogToConsole: true,
      }).on('error', catchError))
      .pipe($.autoprefixer({
        cascade: false,
        remove: false,
        flexbox: true
      }))
      .pipe($.rename({
        extname: '.min.css'
      }))
      .pipe($.cleanCss())
      .pipe(gulp.dest(paths.themeDir))
  ), done();
};

const jsLibs = (done) => {
  return (
      gulp
          .src(paths.buildJsLibs, {allowEmpty: true})
          .pipe($.concat('vendor-libs.js'))
          .pipe($.uglify())
          .on('error', catchError)
          .pipe($.rename({
              extname: '.min.js'
        }))
          .pipe(gulp.dest(paths.themeDir))
  ), done();
};

const js = (done) => {
  return (
    gulp
      .src(paths.buildJs)
      .pipe($.babel({
        presets: [['@babel/preset-env']]
      }))
      .pipe($.concat('scripts.js'))
      .pipe($.uglify())
      .on('error', catchError)
      .pipe($.rename({
        extname: '.min.js'
      }))
      .pipe(gulp.dest(paths.themeDir))
  ), done();
};

const jsEslint = (done) => {
  return (
    gulp
      .src(paths.jsWatch)
      .pipe($.eslint({
        "parserOptions": {
          "ecmaVersion": 6,
        },
        rules: {
          "array-bracket-spacing": [2, "never"],
          "block-scoped-var": 2,
          "brace-style": [2, "1tbs"],
          "camelcase": 1,
          "computed-property-spacing": [2, "never"],
          "curly": 2,
          "eol-last": 2,
          "eqeqeq": [2, "smart"],
          "max-depth": [1, 3],
          "max-len": [1, 120],
          "max-statements": [1, 30],
          "new-cap": 1,
          "no-extend-native": 2,
          "no-mixed-spaces-and-tabs": 2,
          "no-trailing-spaces": [2, {"skipBlankLines": true}],
          "no-unused-vars": 1,
          "no-use-before-define": [2, "nofunc"],
          "object-curly-spacing": [2, "always"],
          "quotes": [2, "single", "avoid-escape"],
          "semi": [2, "always"],
          "keyword-spacing": 2,
          "space-unary-ops": 2
        }
      }))
      .pipe($.eslint.format())
      .pipe($.eslint.failOnError())
      .on('error', catchError)
  ), done();
};

const watch = (done) => {
  gulp.watch(paths.buildScss, gulp.series(scssLint, sass));
  gulp.watch(paths.jsWatch, gulp.series(js, jsEslint));
  done();
};

exports.default = gulp.parallel([scssLint, sass, jsLibs, jsEslint, js, watch]);
exports.build = gulp.parallel([sass, jsLibs, js]);
exports.lint = gulp.parallel([scssLint, jsEslint]);
