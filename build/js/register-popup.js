$(function() {
    const closeButton = document.getElementById('button-close');
    const registerPopup = document.getElementById('register-popup');
    const registerForm = document.getElementById('register-form');
    const backToLogin = document.querySelector('.back');
    const userEmailRegister = document.querySelector('.user-register-email');
    const userPasswordRegister = document.querySelector('.user-register-password');
    const userFirstName = document.querySelector('.user-name');
    const userLastName = document.querySelector('.user-last-name');
    const registerErrors = document.querySelector('.register-errors');
    const loginPopup = document.getElementById('login-popup');

    const closeHandler = (e) => {
        e.preventDefault();
        registerPopup.classList.remove('open');
    };

    const backToLoginHandler = (e) => {
        e.preventDefault();
        registerPopup.classList.remove('open');
        loginPopup.classList.add('open');
    };

    registerForm.addEventListener('submit', e => {
        e.preventDefault();

        register(userEmailRegister.value, userPasswordRegister.value, userFirstName.value, userLastName.value).then(function (html) {
            const errors = $(html).find('.errors');

            if (errors.length > 0) {
                registerErrors.innerHTML = errors[0].innerHTML;

            } else {
                location.reload();
            }
        });

        function register(email, password, firstName, lastName) {
            const data = {
                'customer[email]': email,
                'customer[password]': password,
                'customer[first_name]': firstName,
                'customer[last_name]': lastName,
                form_type: 'create_customer',
                utf8: 'U+2714'
            };

            const promise = $.ajax({
                url: '/account',
                method: 'post',
                data: data,
                dataType: 'html',
                async: true
            });

            return promise;
        }
    });

    closeButton.addEventListener('click', closeHandler);
    backToLogin.addEventListener('click', backToLoginHandler);
});

