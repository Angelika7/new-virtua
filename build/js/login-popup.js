$(function() {
    const loginButton = document.querySelector('.login-button');
    const closeButton = document.getElementById('button-close');
    const popup = document.getElementById('login-popup');
    const registerPopup = document.getElementById('register-popup');
    const loginForm = document.getElementById('login-form');
    const userEmail = document.querySelector('.user-email');
    const userPassword = document.querySelector('.user-password');
    const registerButton = document.querySelector('.register-button');
    const errorsWrapper = document.getElementById('errors');

    const toggleHandler = (e) => {
        e.preventDefault();
        popup.classList.toggle('open');
    };

    const triggerRegisterFormHandler = (e) => {
        e.preventDefault();
        popup.classList.remove('open');
        registerPopup.classList.add('open');
    };

    const closeHandler = (e) => {
        e.preventDefault();
        popup.classList.remove('open');
    };


    loginForm.addEventListener('submit', e => {
        e.preventDefault();

        login(userEmail.value, userPassword.value).then(function (html) {
            const errors = $(html).find('.errors');

            if (errors.length > 0) {
                errorsWrapper.innerHTML = errors[0].innerHTML;
            } else {
                location.reload();
            }
            userPassword.value = '';
            userEmail.value = '';
        });
    });

    function login(email, password) {
        const data = {
            'customer[email]': email,
            'customer[password]': password,
            form_type: 'customer_login',
            utf8: 'U+2714'
        };

        const promise = $.ajax({
            url: '/account/login',
            method: 'post',
            data: data,
            dataType: 'html',
            async: true
        });

        return promise;
    };

    if (loginButton) {
        loginButton.addEventListener('click', toggleHandler);
    }

    closeButton.addEventListener('click', closeHandler);
    registerButton.addEventListener('click', triggerRegisterFormHandler);
});

