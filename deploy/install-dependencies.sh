#!/usr/bin/env bash

apt-get update && apt-get install git gnupg ruby ruby-dev unzip wget zip -y

curl -fsSL https://deb.nodesource.com/setup_14.x | bash -
apt-get install -y nodejs
npm install -g gulp
gem install scss_lint -v 0.57.1

curl -s https://shopify.github.io/themekit/scripts/install.py | python
